package me.mitkovic.android.kotlin.shoestore.shoelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ShoeListViewModel : ViewModel() {

    private val _eventGoToShoeDetailScreen = MutableLiveData<Boolean>()
    val eventGoToShoeDetailScreen: LiveData<Boolean>
        get() = _eventGoToShoeDetailScreen

    fun onGoToShoeDetailScreen() {
        _eventGoToShoeDetailScreen.value = true
    }

    fun goToShoeDetailScreenDone() {
        _eventGoToShoeDetailScreen.value = false
    }

}