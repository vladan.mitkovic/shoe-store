package me.mitkovic.android.kotlin.shoestore.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import me.mitkovic.android.kotlin.shoestore.R
import me.mitkovic.android.kotlin.shoestore.databinding.LoginFragmentBinding

class LoginFragment : Fragment() {

    private lateinit var binding: LoginFragmentBinding
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.login_fragment, container, false
        )

        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        binding.loginViewModel = loginViewModel

        loginViewModel.eventGoToWelcomeScreen.observe(viewLifecycleOwner, {
            if (it) {
                goToWelcomeScreen()
                loginViewModel.goToWelcomeScreenDone()
            }
        })

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.login_screen_title)

        return binding.root
    }

    private fun goToWelcomeScreen() {
        findNavController().navigate(
            LoginFragmentDirections.actionLoginFragmentToWelcomeFragment(
                binding.emailEdit.text.toString()
            )
        )
    }

}