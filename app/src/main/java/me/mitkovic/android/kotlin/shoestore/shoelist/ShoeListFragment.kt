package me.mitkovic.android.kotlin.shoestore.shoelist

import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import me.mitkovic.android.kotlin.shoestore.R
import me.mitkovic.android.kotlin.shoestore.databinding.ShoeListFragmentBinding
import me.mitkovic.android.kotlin.shoestore.shoedetail.ShoeDetailViewModel

class ShoeListFragment : Fragment() {

    private lateinit var binding: ShoeListFragmentBinding
    private lateinit var shoeListViewModel: ShoeListViewModel
    private lateinit var shoeDetailViewModel: ShoeDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.shoe_list_fragment, container, false
        )

        shoeListViewModel = ViewModelProvider(this).get(ShoeListViewModel::class.java)
        binding.shoeListViewModel = shoeListViewModel

        shoeListViewModel.eventGoToShoeDetailScreen.observe(viewLifecycleOwner, {
            if (it) {
                goToShoeDetailScreen()
                shoeListViewModel.goToShoeDetailScreenDone()
            }
        })

        shoeDetailViewModel = ViewModelProvider(requireActivity()).get(ShoeDetailViewModel::class.java)

        shoeDetailViewModel.shoes.observe(viewLifecycleOwner, {

            val textViewLayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            textViewLayoutParams.setMargins(0, 0, 0, 20)

            for (shoe in it) {
                val shoeTextView = TextView(context)

                shoeTextView.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.font_size_small))

                shoeTextView.text = String.format(
                    resources.getString(R.string.shoe_detail),
                    shoe.name,
                    shoe.company,
                    shoe.size,
                    shoe.description
                )
                shoeTextView.layoutParams = textViewLayoutParams
                binding.shoeListLinearLayout.addView(shoeTextView)
            }
        })

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.shoe_list_screen_title)

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)

    }

    private fun goToShoeDetailScreen() {
        findNavController().navigate(ShoeListFragmentDirections.actionShoeListFragmentToShoeDetailFragment())
    }

}