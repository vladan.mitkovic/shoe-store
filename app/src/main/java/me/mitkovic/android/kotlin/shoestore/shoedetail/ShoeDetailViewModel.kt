package me.mitkovic.android.kotlin.shoestore.shoedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.mitkovic.android.kotlin.shoestore.models.Shoe
import timber.log.Timber

class ShoeDetailViewModel : ViewModel() {

    private val _shoes = MutableLiveData<MutableList<Shoe>>(mutableListOf())
    val shoes: LiveData<MutableList<Shoe>>
        get() = _shoes

    private val _eventSaveIsClicked = MutableLiveData<Boolean>()
    val eventSaveIsClicked: LiveData<Boolean>
        get() = _eventSaveIsClicked

    private val _eventCancelIsClicked = MutableLiveData<Boolean>()
    val eventCancelIsClicked: LiveData<Boolean>
        get() = _eventCancelIsClicked

    fun saveCurrentDetail(detail: Shoe?) {
        detail?.let {
            _shoes.value?.add(it)
        }
    }

    fun onSaveIsClicked() {
        _eventSaveIsClicked.value = true
    }

    fun onSaveIsClickedDone() {
        _eventSaveIsClicked.value = false
    }

    fun onCancelIsClicked() {
        _eventCancelIsClicked.value = true
    }

    fun onCancelIsClickedDone() {
        _eventCancelIsClicked.value = false
    }

}