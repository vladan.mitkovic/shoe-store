package me.mitkovic.android.kotlin.shoestore.instructions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import me.mitkovic.android.kotlin.shoestore.R
import me.mitkovic.android.kotlin.shoestore.databinding.InstructionsFragmentBinding

class InstructionsFragment : Fragment() {

    private lateinit var binding: InstructionsFragmentBinding
    private lateinit var instructionsViewModel: InstructionsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.instructions_fragment, container, false
        )

        instructionsViewModel = ViewModelProvider(this).get(InstructionsViewModel::class.java)
        binding.instructionsViewModel = instructionsViewModel

        instructionsViewModel.eventGoToShoeListScreen.observe(viewLifecycleOwner, {
            if (it) {
                goToShoeListScreen()
                instructionsViewModel.goToShoeListScreenDone()
            }
        })

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.instructions_screen_title)

        return binding.root
    }

    private fun goToShoeListScreen() {
        findNavController().navigate(InstructionsFragmentDirections.actionInstructionsFragmentToShoeListFragment())
    }

}