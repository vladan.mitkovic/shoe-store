package me.mitkovic.android.kotlin.shoestore.shoedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import me.mitkovic.android.kotlin.shoestore.R
import me.mitkovic.android.kotlin.shoestore.databinding.ShoeDetailFragmentBinding
import me.mitkovic.android.kotlin.shoestore.models.Shoe

class ShoeDetailFragment : Fragment() {

    private lateinit var binding: ShoeDetailFragmentBinding
    private lateinit var shoeDetailViewModel: ShoeDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.shoe_detail_fragment, container, false
        )

        shoeDetailViewModel = ViewModelProvider(requireActivity()).get(ShoeDetailViewModel::class.java)
        binding.shoeDetailViewModel = shoeDetailViewModel

        shoeDetailViewModel.eventSaveIsClicked.observe(viewLifecycleOwner, {
            if (it) {
                var shoeName = binding.shoeName.text.toString()
                if ("".equals(shoeName)) {
                    Toast.makeText(context, resources.getString(R.string.mandatory_shoe_name), Toast.LENGTH_SHORT).show()
                } else {
                    var shoeSize = binding.shoeSize.text.toString().toDoubleOrNull()
                    var companyName = binding.companyName.text.toString()
                    var shoeDescription = binding.shoeDescription.text.toString()

                    var shoe = Shoe(
                        shoeName,
                        shoeSize,
                        companyName,
                        shoeDescription
                    )

                    shoeDetailViewModel.saveCurrentDetail(shoe)

                    goToShoeListScreen()
                }
                shoeDetailViewModel.onSaveIsClickedDone()
            }
        })

        shoeDetailViewModel.eventCancelIsClicked.observe(viewLifecycleOwner, {
            if (it) {
                goToShoeListScreen()
                shoeDetailViewModel.onCancelIsClickedDone()
            }
        })

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.shoe_detail_screen_title)

        return binding.root
    }

    private fun goToShoeListScreen() {
        findNavController().navigate(ShoeDetailFragmentDirections.actionShoeDetailFragmentToShoeListFragment())
    }

}
