package me.mitkovic.android.kotlin.shoestore.welcome

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WelcomeViewModel(eMail: String) : ViewModel() {

    private val _eventGoToInstructionsScreen = MutableLiveData<Boolean>()
    val eventGoToInstructionsScreen: LiveData<Boolean>
        get() = _eventGoToInstructionsScreen

    private val _email = MutableLiveData<String>()
    val email: LiveData<String>
        get() = _email

    init {
        _email.value = eMail
    }

    fun onGoToInstructionsScreen() {
        _eventGoToInstructionsScreen.value = true
    }

    fun goToInstructionsScreenDone() {
        _eventGoToInstructionsScreen.value = false
    }

}