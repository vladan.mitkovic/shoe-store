package me.mitkovic.android.kotlin.shoestore.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginViewModel : ViewModel() {

    private val _eventGoToWelcomeScreen = MutableLiveData<Boolean>()
    val eventGoToWelcomeScreen: LiveData<Boolean>
        get() = _eventGoToWelcomeScreen

    fun onGoToWelcomeScreen() {
        _eventGoToWelcomeScreen.value = true
    }

    fun goToWelcomeScreenDone() {
        _eventGoToWelcomeScreen.value = false
    }

}