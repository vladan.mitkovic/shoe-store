package me.mitkovic.android.kotlin.shoestore.instructions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class InstructionsViewModel : ViewModel() {

    private val _eventGoToShoeListScreen = MutableLiveData<Boolean>()
    val eventGoToShoeListScreen: LiveData<Boolean>
        get() = _eventGoToShoeListScreen

    fun onGoToShoeListScreen() {
        _eventGoToShoeListScreen.value = true
    }

    fun goToShoeListScreenDone() {
        _eventGoToShoeListScreen.value = false
    }

}