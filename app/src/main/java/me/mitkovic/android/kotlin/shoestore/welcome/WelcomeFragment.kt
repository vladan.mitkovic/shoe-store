package me.mitkovic.android.kotlin.shoestore.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import me.mitkovic.android.kotlin.shoestore.R
import me.mitkovic.android.kotlin.shoestore.databinding.WelcomeFragmentBinding

class WelcomeFragment : Fragment() {

    private lateinit var welcomeViewModel: WelcomeViewModel
    private lateinit var welcomeViewModelFactory: WelcomeViewModelFactory

    private lateinit var binding: WelcomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.welcome_fragment, container, false
        )

        val welcomeFragmentArgs by navArgs<WelcomeFragmentArgs>()

        welcomeViewModelFactory = WelcomeViewModelFactory(welcomeFragmentArgs.email)
        welcomeViewModel =
            ViewModelProvider(this, welcomeViewModelFactory).get(WelcomeViewModel::class.java)
        binding.welcomeViewModel = welcomeViewModel

        welcomeViewModel.eventGoToInstructionsScreen.observe(viewLifecycleOwner, {
            if (it) {
                goToInstructionsScreen()
                welcomeViewModel.goToInstructionsScreenDone()
            }
        })

        welcomeViewModel.email.observe(viewLifecycleOwner, {
            if ("".equals(it)) hideEmail()
        })

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.welcome_screen_title)

        return binding.root
    }

    private fun goToInstructionsScreen() {
        findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToInstructionsFragment())
    }

    private fun hideEmail() {
        binding.email.visibility = View.GONE
    }
}